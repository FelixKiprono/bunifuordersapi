<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //

    protected $fillable = [
        'id','itemid', 'userid', 'quantity','price','total','orderdate'];
    protected $hidden = [
            'id'
        ];
}
