<?php

namespace App\Http\Controllers;
use App\Orders;
use App\Items;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public static function FindUserId($name)
    {
       //check if this user exists
      $result = User::where('name','=',$name)->first();
      if($result)
      {
        return $result->id;
      }
    }

    public static function FindItemId($name)
    {
       //check if this user exists
      $result = Items::where('name','=',$name)->first();
      if($result)
      {
        return $result->id;
      }
    }

    public function FetchTodaysOrder(Request $request)
    {
      //fetch information from client
       $data = $request->all();
       $name = $data['Name'];
    //   $date = $data['Date'];
       //get staff by id
       $userid = OrderController::FindUserId($name);
       $today = date("Y-m-d");
       $result['orders'] = Orders::join('users','users.id','=','orders.userid')
                       ->join('items','items.id','=','orders.itemid')
                     ->where('orders.userid','=',$userid)
                     ->select('items.name as Name','items.description as Description','orders.quantity as Quantity','orders.total as Total')
                     ->where(\DB::Raw('DATE(`orders`.`orderdate`)'), '=', $today)
                     ->get();
                     if($result)
                     {
                       $result['success']="true";
                     }
                     else
                     {
                       $result['success']="false";
                     }
              return response($result);
            }

    public function AddOrder(Request $request)
    {

     //fetch information from client
      $data = $request->all();

      $name = $data['name'];
      $item = $data['itemname'];
      $price = $data['price'];
      $quantity = $data['quantity'];
      $total = $data['total'];
      $date = $data['orderdate'];

      //get user
      $userid = OrderController::FindUserId($name);

      //get item id
      $itemid = OrderController::FindItemId($item);

      //check if this order exists

      //create new order
      $neworder = Orders::create(array("userid" => $userid, "itemid" => $itemid ,"price"=>$price,"quantity"=>$quantity,"total"=>$total,"orderdate"=>$date));
      return response($neworder);
    }
}
