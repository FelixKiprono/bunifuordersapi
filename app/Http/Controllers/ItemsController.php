<?php

namespace App\Http\Controllers;

use App\Items;
use Illuminate\Http\Request;

class ItemsController extends Controller
{

  public function FetchItemsMenu(Request $request)
  {
    //fetch all set items from server
    $result = Items::all();
    if(count($result)>0)
    {
    print(json_encode($result));
    }
  }

public function AddItem(Request $request)
{
  //add new item to database
}

public function DeleteItem(Request $request)
{
  //delete item
}

}
