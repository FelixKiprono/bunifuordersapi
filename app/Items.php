<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    //

    protected $fillable = [
        'id','name', 'price', 'categoryid'];
    protected $hidden = [
            'id'
        ];
}
