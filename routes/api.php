<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Created By Felix (Sergei) Kiprono
| Date : 13th  November 2019
| Company : Bunifu Technologies
| Country : Russian Federation/Kenyan Republic
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::GET('/fetchitems', 'ItemsController@FetchItemsMenu');
Route::POST('/adduser', 'UserController@AddUser');
Route::POST('/addorder', 'OrderController@AddOrder');
Route::POST('/fetchtodayorders', 'OrderController@FetchTodaysOrder');
Route::POST('/login', 'UserController@Login');
